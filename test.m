clear;
clc;

%% Henon map
N = 5000;
x = zeros(N, 2);
x(1, :) = [0.5 ; 0.3];
for i=2: N + 2000
    x(i, 1) = 1 - 1.4  * x(i - 1, 1)^2 + x(i - 1, 2);
    x(i, 2) = 0.3 * x(i - 1, 1);
end
x = x(2000+1: end, :);

%% UCI

% Uci parameters
h = exp(linspace(-10,2,800));
m = 4:2:6;
tau = 1;

% uci
tic
[UCI, UCI2] = uci(x(:,1), m, tau, h, 'Neighbors', 10, 'UCI2_flag', true);
toc


%% Calculate log derivative
wt_name = 'gaus1';
wt_scale = 2;
dlog_uci = zeros(length(m), length(h));
dlog_uci2 = zeros(length(m), length(h));
for i =1:length(m)
    dlog_uci(i, :) = h .* (derivative_cwt(UCI(:,i),wt_name, wt_scale,1,0) ./ ...
                      derivative_cwt(h, wt_name,wt_scale,1,0)) ./ UCI(:,i)';
    dlog_uci2(i, :) = h .* (derivative_cwt(UCI2(:,i),wt_name, wt_scale,1,0) ./ ...
                      derivative_cwt(h, wt_name,wt_scale,1,0)) ./ UCI2(:,i)';
end

%% Plot
figure;
plot(log(h), dlog_uci, '.b');
hold on;
plot(log(h), dlog_uci2, '.r');
plot(log(h),1.25*ones(length(h)),'--k')
hold off;
xlabel('log(h)')
ylabel('Correlation Dimension');
