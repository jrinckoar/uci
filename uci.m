function [UCI, UCI2] = uci(x, m, tau, h, varargin)
%UCI Calculates de U-correlation integral  from a time series.  The code is
% based  on the approach  presented in:  Automatic estimation  of attractor
% invariants by Juan F.  Restrepo and Gastón Schlotthauer.
%
% - https://doi.org/10.1007/s11071-017-3974-3
%
% This algorithm has a modification for fast computing.  It is based on the
% recurrence relation of the incomplete gamma function:
%           Q(a, z) = Q(a+1, z) - (exp(-z) z^a) / gamma(a+1).
% This  means that:
%   UCI_m^{b-2} = UCI_m^{b} - 1/gamma(b/2)  <exp(-z/h) * (z/h)^(b/2-1)>,
% where  b = beta and  <*> is the mean value.
%
% Syntax: [UCI, UCI2] = uci(x, m, tau, varargin)
%
% Inputs:
%   x -- Time series from system X
%   m -- Embedding-dimension / history-length
%   tau -- Embedding delay - time-delay
%   varargin  -- Optional parameters:
%                                'Neighbors' -- Number of temporal Neighbors
%                                              to discard.
%                                'UCI2_flag' -- Calculate UCI2 flag.
% Outputs:
%   UCI -- U-correlation integral for beta = m.
%   UCI2 -- U-correlation integral for b = m - 2.
%
% $Date: 2019-05-08
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.
%--------------------------------------------------------------------------
% Parse parameters and set default values
nn = 15;
UCI2_flag = true;
UCI2 = 1;

nVarargs = length(varargin);
if mod(nVarargs, 2)~=0
    error('Optional input without value');
end
for i=1:2:nVarargs
    switch varargin{i}
        case 'Neighbors'
            nn = varargin{i+1};
            if nn <= 0
                error('Neighbors must be grater than zero.')
            end
        case 'UCI2_flag'
            UCI2_flag = varargin{i+1};
            if ~islogical(UCI2_flag)
                error('UCI2_flag must be logical.')
            end
    end
end

% Normalize time series to zero mean and unitary standard deviation
x = x(:);
x = (x - mean(x)) ./ std(x);

% Parameters
h = h.^2;
H = length(h);
M = length(m);
Le = 10000;

% Set functions for interpolation
t = logspace(-7, 3, Le);
t = [10^(-10), t, 10^5];
GammaInt=zeros(Le + 2, M);
parfor i=1:M
    GammaInt(:,i)=gamcdf(t, m(i)/2, 1, 'upper');
end

UCI  = zeros(H,M);
if(UCI2_flag)
    UCI2  = zeros(H,M);
end

%% Calculate U-correlation integral for beta = m and for beta = m - 2
for i=1:M
    Dv = distanceVector(x, m(i), tau, nn);     % Distances Vector
    Gammafun = GammaInt(:,i);
    Beta = m(i) / 2;
    C = 1 / gamma(Beta);
    parfor k=1:H
        xval = Dv./h(k);
        UCI(k, i) = mean((interp1(t, Gammafun, xval, 'spline')));
        if UCI2_flag
            UCI2(k,i) = UCI(k, i) - C * mean(exp(-xval).*(xval).^(Beta - 1));
        end
    end
end
end
