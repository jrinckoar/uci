function Dv = distanceVector(x, m, tau, nn)
%DISTANCEVECTOR Computes the distances between m-dimensional state vectors.
% This function returns the squared distance vector used to calculate the
% U-correlation integral.
%
% Given  a  time  series   x(i)  i=1,2,...,N.   It  forms  a  sequence  of
% m-dimensional  vectors   Vj=[x(j),x(j+tau),...,x(j+(m-1)tau)]  where
% j= 1,2...,N-(m-1)tau.
%
% Then  the  squared  euclidean  distance  between  vectors  is calculated
% excluding the nn closest neighbors (temporal neighbors).
%
% Syntax: [UCI, UCI2] = uci(x, m, tau, varargin)
%
% Inputs:
%   x -- Time series from system X
%   m -- Embedding-dimension / history-length
%   tau -- Embedding delay - time-delay
%   nn -- Number of temporal Neighbors to be excluded
%
% Outputs:
%   Dv -- Distances Vector
%
% $Date: 2019-05-08
%
% $Author: Juan F. Restrepo, Ph.D.
%
% Copyright (c) 2019, Juan F. Restrepo
% All rights reserved.
%--------------------------------------------------------------------------
L = length(x) - (m-1) * tau;

%% Form the m-dimensional state vectors
V = zeros(L, m);
for i=1:L
    V(i,:)=x(i:tau:i + (m-1) * tau);
end

%% Calculate the distances
Dv = zeros((L - nn)*(L - nn - 1)/2, 1);
K = nn:L-1;
ind1 = L-K;
ind2 = K+1;
ind = [0,cumsum(ind1(1:end))];
for k=1: L - 1 - nn
    Dv((ind(k)+1:ind(k+1)))=...
        dot(V(1:ind1(k), :)',V(1:ind1(k), :)',1)+...
        dot(V(ind2(k):L, :)',V(ind2(k):L, :)',1)...
        -2*dot(V(1:ind1(k), :)',V(ind2(k):L, :)',1);
end
