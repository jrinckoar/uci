--------------------------------------------------------------------------------------------------------------------
# U-Correlation Integral -- MATLAB CODE
## Juan F. Restrepo
### jrestrepo@ingenieria.uner.edu.ar

*Laboratorio de Señales y Dinámicas no Lineales,  Instituto de Bioingeniería y Bioinformática, CONICET - Universidad
Nacional de Entre Ríos.  Ruta prov 11 km 10 Oro Verde, Entre Ríos, Argentina.*

--------------------------------------------------------------------------------------------------------------------
## Abstract
UCI Calculates de U-correlation integral  from a time series and the coarse grained  estimators for the noise level,
the correlation dimension and  the correlation entropy.  The code is based on  the approach presented in:  Automatic
estimation of attractor invariants by Juan F.  Restrepo and Gastón Schlotthauer.

- https://doi.org/10.1007/s11071-017-3974-3

Other readings:  

- https://doi.org/10.1103/PhysRevE.94.012212  
- https://doi.org/10.1155/2018/2173640

### Instructions

### Usage

 uci: Calculates de U-correlation integral  from a time series

##### Syntax: 
  [UCI, UCI2] = uci(x, m, tau, varargin)

##### Inputs:
  * x -- Time series from system X
  * m -- Embedding-dimension / history-length
  * tau -- Embedding delay - time-delay
  * varargin  -- Optional parameters:   nn -- Number of temporal Neighbors to be excluded.  
                                        UCI2 -- Calculate UCI2 flag.
##### Outputs:
  * UCI -- U-correlation integral for beta = m.
  * UCI2 -- U-correlation integral for b = m - 2.

### Files
1. uci.m
2. distanceVector


--------------------------------------------------------------------------------------------------------------------
## Citing Information:
@Article{Restrepo2018,
  author={Restrepo, Juan F. and Schlotthauer, Gast{\'o}n},  
  title={Automatic estimation of attractor invariants},  
  journal={Nonlinear Dynamics",  
  year={2018},  
  month={Feb},  
  volume={91},  
  number={3},  
  pages={1681--1696},  
  issn={1573-269X},  
  doi={10.1007/s11071-017-3974-3},  
}

--------------------------------------------------------------------------------------------------------------------
Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
